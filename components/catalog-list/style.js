import { StyleSheet, Platform } from 'react-native';
import { APP_COLORS } from '../../styles/colors';

export const style = StyleSheet.create({
    card: {
        marginTop: 0,
        marginBottom: 20,
        borderWidth: 1,
        borderRadius: 10,
        width: 315,
        backgroundColor: APP_COLORS.White,
        borderWidth: 0,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOffset: {height: 2, width: 0},
                shadowOpacity: 1,
                shadowRadius: 1
            },
            android: {
                elevation: 1
            }
        })
    },
    content: {
        padding: 20,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-around'
    },
    image: {
        justifyContent: 'center',
        zIndex: 0
    },
    name: {
        fontSize: 24,
        lineHeight: 24,
        color: APP_COLORS.DeepCarmine,
        fontWeight: 'bold'
    },
    winemaker: {
        paddingTop: 4,
        paddingBottom: 4,
        fontSize: 16,
        fontWeight: '200',
        lineHeight: 16
    },
    aop: {
        marginTop: 11,
        marginBottom: 20,
        fontSize: 10,
        fontWeight: 'bold',
        lineHeight: 12,
        color: APP_COLORS.Charcoal
    },
    desc: {
        fontSize: 18,
        fontStyle: 'italic',
        fontWeight: '200',
        lineHeight: 22,
        color: APP_COLORS.Charcoal
    },
    entitled: {
        marginLeft: 20,
        marginRight: 20,
        paddingTop: 12,
        borderTopWidth: 1,
        borderTopColor: APP_COLORS.GainsBoro,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignSelf: 'stretch'
    },
    priceText: {
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 15
    },
    priceTextSubscriber: {
        fontWeight: '500',
    },
    row: {
        marginLeft: 20,
        marginRight: 20,
        paddingBottom: 12,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        alignSelf: 'stretch'
    },
    price: {
        fontSize: 18,
        fontWeight: 'bold',
        lineHeight: 23
    },
    label: {
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        height: 25,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    labelText: {
        textAlign: 'center',
        lineHeight: 24,
        color: APP_COLORS.White,
        fontSize: 12,
        fontWeight: 'bold'
    },
    cadetGrey: {
        color: APP_COLORS.CadetGrey
    }
});
