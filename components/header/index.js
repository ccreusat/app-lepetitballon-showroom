import React from 'react';
import { View, Image } from 'react-native';
import { style } from './style';

const Header = () => (
    <View>
        <View style={style.header}>
            <Image
                style={{width:40, height:40}}
                source={require('./logo-small.png')}
            />
        </View>
    </View>
);

export default Header;
