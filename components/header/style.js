import { StyleSheet } from 'react-native';
import { APP_COLORS } from '../../styles/colors';

export const style = StyleSheet.create({
    header: {
        paddingTop: 40,
        backgroundColor: APP_COLORS.White,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    }
});