import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, TouchableHighlight, Alert, Modal, Dimensions} from 'react-native';
import { Card } from 'react-native-elements';
import { style } from './style';
import Helper from '../../utils/Helper.js';
import API from '../../utils/Api.js'
import Carousel from 'react-native-snap-carousel';
//import ModalExample from '../ModalExample.js'

export default class CatalogList extends React.Component {

  state = {
     modalVisible: false,
     productInfo: []
  }

async toggleModal(visible, id) {
     const info = await API.getProductInfo(id)
     this.setState({
       modalVisible: visible,
       productInfo: info
   })
  }

  sliderBuilder(){
    const info = this.state.productInfo
    let content = {
      'A':{
        'content':<View  style={style.card}>
            <View style={style.content}>
                <View style={{width: 170, zIndex: 2}}>
                    <Text style={style.name}>{this.state.productInfo.name}</Text>
                    <Text style={style.winemaker}>{this.state.productInfo.domain}</Text>
                    <Text style={style.aop}>{this.state.productInfo.appellation}</Text>
                    <Text></Text>
                    <Text style={style.desc}>{this.state.productInfo.dashboard_description}</Text>
                </View>
                <Image
                    style={style.image}
                    resizeMode="cover"
                    source={require('./marianne-wine-estate-okoma-2016.jpg')}
                />
            </View>
            <View style={style.entitled}>
                <Text style={[style.priceText, style.cadetGrey]}>Public Price</Text>
                <Text style={style.priceText}>Subscriber Price</Text>
            </View>
            <View style={style.row}>
                <Text style={[style.price, style.cadetGrey]}>{this.state.productInfo.price}€</Text>
                <Text style={style.price}>{this.state.productInfo.subscriber_price}€</Text>
            </View>
            <View style={[{backgroundColor: Helper.getLabelColour(359)}, style.label]}>
                <Text style={style.labelText}>{Helper.getLabelText(359)}</Text>
            </View>
        </View>
      },
      'B':{
        'content':'Hello'
      }
    }
    return content
  }

  _renderItem ({item, index}) {
       return (
           <View style={styles.slide}>
               <Text style={styles.title}>{ item.content }</Text>
           </View>
       );
   }

    wp (percentage) {
     const value = (percentage * Dimensions.get('window').width) / 100
     return Math.round(value)
 }

 render() {
   const viewportWidth = Dimensions.get('window').width
    const productList = Object.values(this.props.list)
    return(
        <Card containerStyle={styles.container}>
            <View style={{ padding:20, margin:0, flexDirection: 'row', flexWrap: 'wrap', flex: 1, justifyContent: 'space-between' }}>{
            productList.map((p, i) => {
                 return (
                  <TouchableHighlight key={i} onPress = {() => {this.toggleModal(true, p.id)}}>
                  <View  style={style.card}>
                      <View style={style.content}>
                          <View style={{width: 170, zIndex: 2}}>
                              <Text style={style.name}>{p.name}</Text>
                              <Text style={style.winemaker}>{p.domain}</Text>
                              <Text style={style.aop}>{p.appellation}</Text>
                              <Text></Text>
                              <Text style={style.desc}>{p.dashboard_description}</Text>
                          </View>
                          <Image
                              style={style.image}
                              resizeMode="cover"
                              source={require('./marianne-wine-estate-okoma-2016.jpg')}
                          />
                      </View>
                      <View style={style.entitled}>
                          <Text style={[style.priceText, style.cadetGrey]}>Public Price</Text>
                          <Text style={style.priceText}>Subscriber Price</Text>
                      </View>
                      <View style={style.row}>
                          <Text style={[style.price, style.cadetGrey]}>{p.price}€</Text>
                          <Text style={style.price}>{p.subscriber_price}€</Text>
                      </View>
                      <View style={[{backgroundColor: Helper.getLabelColour(p.label)}, style.label]}>
                          <Text style={style.labelText}>{Helper.getLabelText(p.label)}</Text>
                      </View>
                  </View>
                  </TouchableHighlight>
                )}
              )}
            </View>

            <View style = {styles.modal_cont}>
               <Modal animationType = {"slide"} transparent = {false}
                  visible = {this.state.modalVisible}
                  onRequestClose = {() => { console.log("Modal has been closed.") } }>
                  <View style = {styles.modal}>
                    <Carousel
                       ref={(c) => { this._carousel = c; }}
                       data={Object.values(this.sliderBuilder())}
                       renderItem={this._renderItem}
                       sliderWidth={viewportWidth}
                       itemWidth={this.wp(75) + this.wp(2) * 2}
                     />
                     <TouchableHighlight onPress = {() => {
                        this.toggleModal(!this.state.modalVisible)}}>
                        <Text style = {style.LabelText}>Close Modal</Text>
                     </TouchableHighlight>
                  </View>
               </Modal>
            </View>

        </Card>
      )
    }
  }
export default CatalogList;

const styles = StyleSheet.create({
  container: {
    borderWidth:0,
    shadowOpacity:0,
    padding: 0,
    margin: 0,
    backgroundColor: 'transparent'
  },
  modal_cont: {
     alignItems: 'center',
     backgroundColor: '#ede3f2'
  },
  modal: {
     flex: 1,
     alignItems: 'center',
     backgroundColor: '#02A0AE',
     padding: 100
  }
});
