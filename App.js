import React from 'react';
import { StyleSheet, View, ScrollView, Text } from 'react-native';
import Header from './components/header';
import CatalogList from './components/catalog-list';
import Search from './components/search';
import API from './utils/Api.js';

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      catalogList,
      productList: [],
      loading : true
    }
  }

  async componentDidMount(){
    
    //console.log(products)
    this.setState({
      productList : await API.getProducts(5),
      loading: false
    })

  }

  render() {
    return (
      <View style={styles.container}>
        <Header />
        <Search wineColor={this.state.wine_colour}/>
        <ScrollView>
          if (this.state.loading){
            <Text> "Loading ..." </Text>
          }
          else <CatalogList list={this.state.productList} />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#9FD4D9'
  }
});
