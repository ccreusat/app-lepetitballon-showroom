
class Helper {

  getLabelText(id){
    const labels = require('../static-data/labels.json');
    for (let k in labels){
      if (labels[k].value == id){
        return labels[k].label
      }
    }
  }

  getLabelColour(id){
    if (!id){
      return "#f0f0f0"
    }
    else {
      const labels = require('../static-data/labels.json');
      for (let k in labels){
        if (labels[k].value == id){
          return labels[k].hex
        }
      }
    }
  }

  getOccassion(ids){
    const occasion = require('../static-data/occasion.json');
    var arrayOccasions = []
    for (var o in ids) {
      for (var k in occasion){
        if (o == occasion[k].value){
          arrayOccasions.push(occasion[k].label)
        }
      }
    }
    return arrayOccasions
  }

  getWineColour(id){
    const wineColour = require('../static-data/wine_colour.json');
    for (let k in labels){
      if (labels[k].value == id){
        return labels[k].label
      }
    }
  }

  getWineOrigin(id){
      const origin = require('../static-data/origin.json');
      for (let k in labels){
        if (labels[k].value == id){
          return labels[k].label
        }
      }
  }

}

export default new Helper();
