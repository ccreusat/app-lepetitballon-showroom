const BASE_API_URL = 'https://www.lepetitballon.com/privateapi/v1/shop';

class Api {
  async getProducts(shop_id){
    const request = new Request(`${BASE_API_URL}/products/${shop_id}`, {
      method: 'GET',
      headers: new Headers({
        'TOKEN': 'MyTestToken'
      })
    })
    const query = await fetch(request)
    const data = await query.json()
    //console.log(data)
    return data
  }

  async getProductInfo(product_id){
    const request = new Request(`${BASE_API_URL}/info/${product_id}`, {
      method: 'GET',
      headers: new Headers({
        'TOKEN': 'MyTestToken'
      })
    })
    const query = await fetch(request)
    const data = await query.json()
    //console.log(data)
    return data
  }
}

export default new Api();
